import tkinter as tk
from tkinter import messagebox
import tkinter.font as font
from time import strftime
from datetime import date
from geopy.geocoders import Nominatim
from timezonefinder import TimezoneFinder
import datetime
import pytz
import geopandas as gpd
from shapely.geometry import Point
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure


class MainW():
    def __init__(self, window):
        self.geolocator = Nominatim(user_agent="working_in_different_timezone_is_annoying")
        global is_on_map
        is_on_map = False
        self.window = window
        self.my_font = font.Font(size=14)
        self.window.title("Timezone traveling")
        # tkinter things labels, entry ...
        self.countries_frame = tk.Frame(self.window)
        self.date_label = tk.Label(self.window, font=self.my_font)
        self.time_label = tk.Label(self.window, font=self.my_font)
        self.title_label = tk.Label(self.window, font=self.my_font, text="Type in a place:")
        self.place_entry = tk.Entry(self.window, font=self.my_font)

        self.place_label = tk.Label(self.window, font=self.my_font)
        # plot the map
        self.fig = Figure(figsize=(5, 3), dpi=100, frameon=False, facecolor='g')
        self.ax = self.fig.add_subplot(111)
        self.ax.set_axis_off()
        self.map_canvas = FigureCanvasTkAgg(self.fig, master=self.window)
        self.place_entry.bind("<Return>", lambda event: self.main())
        # Position of the widgets on the windows
        self.countries_frame.grid(row=1, column=3, rowspan=5, columnspan=7, sticky="nsew")
        self.date_label.grid(row=0, column=0, padx=5, sticky="nse")
        self.time_label.grid(row=0, column=1, padx=5, sticky="nsw")
        self.title_label.grid(row=1, column=0, columnspan=2, padx=5, sticky="nsw")
        self.place_entry.grid(row=2, column=0, columnspan=2, padx=5, pady=5, sticky="new")

        self.place_label.grid(row=3, column=0, columnspan=2, padx=5, pady=5, sticky="nsew")
        self.map_canvas.get_tk_widget().grid(row=0, column=10, rowspan=6, columnspan=12, padx=5, pady=5)
        self.cities = []
        self.ref = [*range(24)]
        self.place_array = []
        self.city_array_label = []
        self.ref_time_array = []
        self.country_array_label = []
        self.map()
        # show the date and time
        self.today_date()
        self.digitalclock()

    def main(self):
        place = self.place_entry.get()
        self.verify_place(place)
        global is_on_map
        self.place_title_label = tk.Label(self.window, font=self.my_font, text="Pick the right place:")
        self.place_title_label.grid(row=0, column=3, columnspan=7, padx=5, pady=2, sticky="nsew")
        if is_on_map is True:
            array = self.available_places(place)
            self.place_label.config(text=place)
            for index in range(len(array)):
                self.country_label = tk.Label(self.countries_frame, font=self.my_font, relief='groove')
                self.country_array_label.append(self.country_label)
                self.country_array_label[-1].grid(row=index, column=0, sticky="nsew")
                self.country_label.configure(text=array[index])
                self.country_label.bind("<Button-1>", lambda event,
                                        i=(index): self.add_line(place + " " + array[i]))
        self.clear_text()

    def verify_place(self, place):
        global is_on_map
        try:
            location = self.geolocator.geocode(place)
            is_on_map = True
            return location.address
        except:
            is_on_map = False
            messagebox.showerror("showerror", "Can't find the place !")

    def available_places(self, place):
        possible_locations = self.geolocator.geocode(place, exactly_one=False)
        full_address = []
        address_part_1 = []
        adress_part_2 = []
        for location in possible_locations:
            loc_dict = location.raw
            address_part_1.append(loc_dict['display_name'].rsplit(', ', 3)[1])
            adress_part_2.append(loc_dict['display_name'].rsplit(', ', 1)[1])
        country = [x + " " + y for (x, y) in zip(address_part_1, adress_part_2)]
        [full_address.append(x) for x in country if x not in full_address]
        return full_address

    def add_line(self, places):
        self.cities.append(places)
        array_length = len(self.cities)
        last_place = self.cities[array_length - 1]
        place = self.get_place(places)
        self.place_array.append(place)
        self.city_label = tk.Label(self.window, font=self.my_font)
        self.city_array_label.append(self.city_label)
        self.city_array_label[-1].grid(row=array_length + 7, column=0, columnspan=2, pady=(0, 1), sticky="nsew")
        self.city_label.configure(text=last_place)
        if array_length > 1:
            rotate_time = self.rotate_list(array_length)
            for j in self.ref:
                self.ref_time_label = tk.Label(self.window, font=self.my_font, relief='solid', width=3)
                self.ref_time_array.append(self.ref_time_label)
                self.ref_time_array[-1].grid(row=array_length + 7, column=j + 2, sticky="nsew")
                self. ref_time_label.configure(text=rotate_time[j])
        else:
            for j in self.ref:
                self.ref_time_label = tk.Label(self.window, font=self.my_font, relief='solid', width=3)
                self.ref_time_array.append(self.ref_time_label)
                self.ref_time_array[-1].grid(row=array_length + 7, column=j + 2, sticky="nsew")
                self. ref_time_label.configure(text=self.ref[j])

    def get_place(self, place):
        """ For a given place, find the timezone, then return the offset time"""
        location = self.geolocator.geocode(place, exactly_one=True)
        long = []
        lat = []
        # arrays for the dot markers on the map
        long.append(location.longitude)
        lat.append(location.latitude)
        # function to plot the dots marker on the map
        self.map_markers(long, lat)
        obj = TimezoneFinder()
        country_timezone = obj.timezone_at(lng=location.longitude, lat=location.latitude)
        return country_timezone

    def get_offset(self, country_timezone):
        # from the timezone find the offset time
        string_timezone = pytz.timezone(country_timezone)
        offset = datetime.datetime.now(string_timezone).strftime('%z')
        return offset

    def time_diff(self, offset_my_country, offset_other_country):
        """ Make the difference between two timezone adapt the format to have only the hours
        return the number of hours difference"""
        difference = int(offset_other_country) - int(offset_my_country)
        if len(str(difference)) == 3:
            diff = int(str(difference)[:1])
        else:
            diff = int(str(difference)[:2])
        return diff

    def rotate_list(self, element):
        """ Take the first element of the cities array,and a second one
        then from the timezone offsett, make the difference between the offsse and rotate the time"""
        my_country = self.get_offset(self.place_array[0])
        other_country = self.get_offset(self.place_array[element - 1])
        diff = self.time_diff(my_country, other_country)
        rotate_time = self.ref[diff:] + self.ref[:diff]
        return rotate_time

    def map(self):
        world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))
        world.boundary.plot(ax=self.ax, color='gray')
        self.fig.canvas.draw()

    def map_markers(self, long, lat):
        geometry = [Point(xy) for xy in zip(long, lat)]
        self.geo_df = gpd.GeoDataFrame(geometry=geometry)
        self.ax = self.geo_df.plot(ax=self.ax, markersize=7, color='red', marker='*', zorder=3)
        self.fig.canvas.draw()

    def today_date(self):
        """ Show the today date in a label """
        self.day = date.today()
        self.today = self.day.strftime("%d/%m/%Y")
        self.date_label.config(text=str(self.today))

    def digitalclock(self):
        """Show the time in a label"""
        string = strftime('%H:%M:%S %p')
        self.time_label.config(text=string)
        self.time_label.after(1000, self.digitalclock)

    def clear_text(self):
        self.place_entry.delete(0, 'end')

    def quit(self):
        self.window.quit
        self.window.destroy()


def main():
    root = tk.Tk()
    app = MainW(root)
    root.mainloop()


if __name__ == '__main__':
    main()
