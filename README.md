# tkinter_timezone_converter


![alt text](images/time_travel_1.png "GUI")

Sometimes you might get more than one result, click to choose the correct one.
![alt text](images/time_travel_2.png "GUI")

The first place is the reference to know the time offset when other places are added.  
![alt text](images/time_travel_3.png "GUI")

Time offset between the two places.  
![alt text](images/time_travel_4.png "GUI")

## Usage

```bash
python time_travel.py
```
